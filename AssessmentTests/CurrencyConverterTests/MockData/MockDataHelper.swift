//
//  MockDataHelper.swift
//  AssessmentTests
//
//  Created by Amol Bombe on 27/03/22.
//

@testable import Assessment
import Foundation

class MockDataHelper {
    func getData()-> Response? {
        var response: Response?
        var data: Data?
        if let path = Bundle(for: type(of: self)).path(forResource: "MockData", ofType: "json") {
            do {
                data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                response = try JSONDecoder().decode(Response.self, from: data!)
            } catch {}
        }
        
        return response
    }
}
