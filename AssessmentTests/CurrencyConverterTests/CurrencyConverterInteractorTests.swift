//
//  CurrencyConverterInteractorTests.swift
//  AssessmentTests
//
//  Created by Amol Bombe on 27/03/22.
//

@testable import Assessment
import XCTest

class CurrencyConverterInteractorTests: XCTestCase {
    var interactor: CurrencyConverterInteractor!
    var view: CurrencyConverterViewControllerStub!
    var presenter: CurrencyConverterPresenter!
    var router: CurrencyConverterRouterStub!
    private var expectation: XCTestExpectation!

    override func setUp() {
        view = CurrencyConverterViewControllerStub(expectation)
        interactor = CurrencyConverterInteractor()
        router = CurrencyConverterRouterStub(expectation)
        
        presenter = CurrencyConverterPresenter(view: view, interactor: interactor, router: router)

        interactor.presenter = presenter
        interactor.response = MockDataHelper().getData()
        interactor.updateResponse()
        expectation = XCTestExpectation(description: "expectation")
    }
    
    override class func tearDown() {
        
    }
    
    func testCreateListViewModels() {
        interactor.createListViewModels()
        XCTAssertNotNil(interactor.listViewModels)
    }
    
    func testUpdateConvertedAmount() {
        interactor.updateConvertedAmount()
        XCTAssertTrue(view.audLabelTextUpdated)
    }
    
    func testFetchCurrencyData() {
        NetworkHelper.shared.isMock = true
        interactor.fetchCurrencyData()
        XCTAssertNotNil(interactor.response)
    }
    
    func testUpdateSelectedCurrencyRate() {
        interactor.updateSelectedCurrencyRate(index: 0, name: "name")
        XCTAssertTrue(view.selectedCurrencyTextUpdated)
    }
    
    func testUpdateAmountToConvert() {
        interactor.updateAmountToConvert(amount: "10")
        XCTAssertNotNil(interactor.amount)
    }
    
    func testShowCurrencyListSelction() {
        interactor.showCurrencyListSelction()
        XCTAssertTrue(router.shownCurrencyList)
    }
}
