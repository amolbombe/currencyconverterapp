//
//  CurrencyConverterViewControllerStub.swift
//  AssessmentTests
//
//  Created by Amol Bombe on 27/03/22.
//

@testable import Assessment
import XCTest

class CurrencyConverterViewControllerStub: CurrencyConverterVC {
    private var expectation: XCTestExpectation!
    private(set) var audLabelTextUpdated: Bool = false
    private(set) var selectedCurrencyTextUpdated: Bool = false

    init(_ expectation: XCTestExpectation? = nil) {
        self.expectation = expectation
    }
    
    func updateAUDLabelText(text: String) {
        audLabelTextUpdated = true
//        expectation.fulfill()
    }
    
    func updateSelectedCurrencyText(text: String) {
        selectedCurrencyTextUpdated = true
//        expectation?.fulfill()
    }
    
    func getViewController() -> UIViewController? {
        return UIViewController()
    }
}
