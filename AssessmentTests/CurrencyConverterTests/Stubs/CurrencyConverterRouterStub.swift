//
//  CurrencyConverterRouterStub.swift
//  AssessmentTests
//
//  Created by Amol Bombe on 27/03/22.
//

@testable import Assessment
import XCTest

class CurrencyConverterRouterStub: CurrencyConverterRouterProtocol {
    private var expectation: XCTestExpectation!
    private(set) var shownCurrencyList: Bool = false

    init(_ expectation: XCTestExpectation? = nil) {
        self.expectation = expectation
    }
    
    func showCurrencyList(viewcontroller: UIViewController, listViewModels: [ListViewModel], selection: @escaping (String, Int) -> Void) {
        shownCurrencyList = true
        expectation?.fulfill()
    }
}
