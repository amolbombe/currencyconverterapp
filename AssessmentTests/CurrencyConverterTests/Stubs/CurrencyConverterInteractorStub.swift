//
//  CurrencyConverterInteractorStub.swift
//  AssessmentTests
//
//  Created by Amol Bombe on 27/03/22.
//

@testable import Assessment
import XCTest

class CurrencyConverterInteractorStub: CurrencyConverterInteractorProtocol {
    private var expectation: XCTestExpectation!
    private var fetchedCurrencyData: Bool = false
    private var updatedSelectedCurrencyRate: Bool = false
    private var updatedAmountToConvert: Bool = false
    private var shownCurrencyListSelction: Bool = false

    init(_ expectation: XCTestExpectation? = nil) {
        self.expectation = expectation
    }
    
    func fetchCurrencyData() {
        fetchedCurrencyData = true
        expectation.fulfill()
    }
    
    func updateSelectedCurrencyRate(index: Int, name: String) {
        updatedSelectedCurrencyRate = true
        expectation.fulfill()
    }
    
    func updateAmountToConvert(amount: String?) {
        updatedAmountToConvert = true
        expectation.fulfill()
    }
    
    func showCurrencyListSelction() {
        shownCurrencyListSelction = true
        expectation.fulfill()
    }
    
    
}
