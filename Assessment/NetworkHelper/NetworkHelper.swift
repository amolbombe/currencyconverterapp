//
//  NetworkHelper.swift
//  Assessment
//
//  Created by Amol Bombe on 21/03/22.
//

import Foundation

protocol NetworkHelperProtocol {
    func request<T: Codable>(completion: @escaping (Result<T, Error>) -> ())
}

class NetworkHelper: NetworkHelperProtocol {
    
    static let shared = NetworkHelper()
    var isMock = false
    private init() {}
    
    func request<T: Codable>(completion: @escaping (Result<T, Error>) -> ()) {
        if isMock {
            completion(.success(getData() as! T))
        }
        let url = URL(string: "https://www.westpac.com.au/bin/getJsonRates.wbc.fx.json")
        let urlRequest = URLRequest(url: url!)

        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            if let err = error {
                completion(.failure(err))
                print(err.localizedDescription)
                return
            }
            guard response != nil, let data = data else {
                return
            }
            let responseObject = try! JSONDecoder().decode(T.self, from: data)
            DispatchQueue.main.async {
                completion(.success(responseObject))
            }
        }
        dataTask.resume()
    }
    
    func getData()-> Response? {
        var response: Response?
        var data: Data?
        if let path = Bundle(for: type(of: self)).path(forResource: "MockData", ofType: "json") {
            do {
                data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                response = try JSONDecoder().decode(Response.self, from: data!)
            } catch {}
        }
        
        return response
    }
}
