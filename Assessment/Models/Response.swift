//
//  Response.swift
//  Assessment
//
//  Created by Amol Bombe on 21/03/22.
//

import Foundation

//   let response = try? newJSONDecoder().decode(Response.self, from: jsonData)

import Foundation

// MARK: - Response
struct Response: Codable {
    let apiVersion: String
    let status: Int
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let brands: Brands

    enum CodingKeys: String, CodingKey {
        case brands = "Brands"
    }
}

// MARK: - Brands
struct Brands: Codable {
    let wbc: Wbc

    enum CodingKeys: String, CodingKey {
        case wbc = "WBC"
    }
}

// MARK: - Wbc
struct Wbc: Codable {
    let brand: String
    let portfolios: Portfolios

    enum CodingKeys: String, CodingKey {
        case brand = "Brand"
        case portfolios = "Portfolios"
    }
}

// MARK: - Portfolios
struct Portfolios: Codable {
    let fx: Fx

    enum CodingKeys: String, CodingKey {
        case fx = "FX"
    }
}

// MARK: - Fx
struct Fx: Codable {
    let portfolioID: String
    let products: Products

    enum CodingKeys: String, CodingKey {
        case portfolioID = "PortfolioId"
        case products = "Products"
    }
}

// MARK: - Products
struct Products: Codable {
    let usd: Usd
    let eur: Eur
    let gbp: Gbp
    let nzd: Nzd
    let cny: Cny
    let aed: Aed
    let ars: Ars
    let bdt: Bdt
    let bnd: Bnd
    let brl: Brl
    let cad: CAD
    let chf: Chf
    let clp: Clp
    let dkk: Dkk
    let fjd: Fjd
    let hkd: Hkd
    let idr: Idr
    let inr: Inr
    let jpy: Jpy
    let krw: Krw
    let lkr: Lkr
    let myr: Myr
    let nok: Nok
    let pgk: Pgk
    let php: PHP
    let pkr: Pkr
    let sar: Sar
    let sbd: Sbd
    let sek: Sek
    let sgd: Sgd
    let thb: Thb
    let top: Top
    let twd: Twd
    let vnd: Vnd
    let vuv: Vuv
    let wst: Wst
    let xpf: Xpf
    let zar: Zar
    let xau: Xau
    let cnh: Cnh

    enum CodingKeys: String, CodingKey {
        case usd = "USD"
        case eur = "EUR"
        case gbp = "GBP"
        case nzd = "NZD"
        case cny = "CNY"
        case aed = "AED"
        case ars = "ARS"
        case bdt = "BDT"
        case bnd = "BND"
        case brl = "BRL"
        case cad = "CAD"
        case chf = "CHF"
        case clp = "CLP"
        case dkk = "DKK"
        case fjd = "FJD"
        case hkd = "HKD"
        case idr = "IDR"
        case inr = "INR"
        case jpy = "JPY"
        case krw = "KRW"
        case lkr = "LKR"
        case myr = "MYR"
        case nok = "NOK"
        case pgk = "PGK"
        case php = "PHP"
        case pkr = "PKR"
        case sar = "SAR"
        case sbd = "SBD"
        case sek = "SEK"
        case sgd = "SGD"
        case thb = "THB"
        case top = "TOP"
        case twd = "TWD"
        case vnd = "VND"
        case vuv = "VUV"
        case wst = "WST"
        case xpf = "XPF"
        case zar = "ZAR"
        case xau = "XAU"
        case cnh = "CNH"
    }
    
    func getAllProductsArray() -> [ProductsProtocol] {
        [usd,eur,gbp,nzd,cny,aed,ars,bdt,bnd,brl,cad,chf,clp,dkk,fjd,hkd,idr,inr,jpy,krw,lkr,myr,nok,pgk,php,pkr,sar,sbd,sek,sgd,thb,top,twd,vnd,vuv,wst,xpf,zar,xau,cnh]
    }
}

protocol ProductsProtocol {
    var productID: String { get set }
    
    func getProductsDetails() -> ProductsDetailsProtocol
}

protocol ProductsDetailsProtocol {
    var currencyCode: String { get set }
    var currencyName: String { get set }
    var country: String { get set }
    var buyTT: String { get set }
    var sellTT: String { get set }
}

// MARK: - Aed
struct Aed: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.aed
    }
    
    var productID: String
    let rates: AEDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - AEDRates
struct AEDRates: Codable {
    let aed: ARSClass

    enum CodingKeys: String, CodingKey {
        case aed = "AED"
    }
}

// MARK: - ARSClass
struct ARSClass: Codable,ProductsDetailsProtocol {
    var currencyCode, currencyName, country, buyTT: String
    var sellTT: String
    let buyTC, buyNotes, sellNotes: String
    let spotRateDateFmt: String
    let effectiveDateFmt, updateDateFmt: String
    let lastupdated: String

    enum CodingKeys: String, CodingKey {
        case currencyCode, currencyName, country, buyTT, sellTT, buyTC, buyNotes, sellNotes
        case spotRateDateFmt = "SpotRate_Date_Fmt"
        case effectiveDateFmt = "effectiveDate_Fmt"
        case updateDateFmt = "updateDate_Fmt"
        case lastupdated = "LASTUPDATED"
    }
}

enum BuyNotes: String, Codable {
    case empty = ""
    case nA = "N/A"
}

enum EDateFmt: String, Codable {
    case the20160901T080001841000 = "20160901T080001,84+10:00"
    case the20160901T085002111000 = "20160901T085002,11+10:00"
    case the20161011T151205921100 = "20161011T151205,92+11:00"
    case the20220318T080604741100 = "20220318T080604,74+11:00"
}

enum Lastupdated: String, Codable {
    case the0310PM11Oct2016 = "03:10 PM 11 Oct 2016"
    case the0803AM18Mar2022 = "08:03 AM 18 Mar 2022"
    case the0809AM01Sep2016 = "08:09 AM 01 Sep 2016"
}

// MARK: - Ars
struct Ars: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.ars
    }
    
    var productID: String
    let rates: ARSRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - ARSRates
struct ARSRates: Codable {
    let ars: ARSClass

    enum CodingKeys: String, CodingKey {
        case ars = "ARS"
    }
}

// MARK: - Bdt
struct Bdt: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.bdt
    }
    
    var productID: String
    let rates: BDTRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - BDTRates
struct BDTRates: Codable {
    let bdt: ARSClass

    enum CodingKeys: String, CodingKey {
        case bdt = "BDT"
    }
}

// MARK: - Bnd
struct Bnd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.bnd
    }
    
    var productID: String
    let rates: BNDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - BNDRates
struct BNDRates: Codable {
    let bnd: ARSClass

    enum CodingKeys: String, CodingKey {
        case bnd = "BND"
    }
}

// MARK: - Brl
struct Brl: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.brl
    }
    
    var productID: String
    let rates: BRLRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - BRLRates
struct BRLRates: Codable {
    let brl: ARSClass

    enum CodingKeys: String, CodingKey {
        case brl = "BRL"
    }
}

// MARK: - CAD
struct CAD: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.cad
    }
    
    var productID: String
    let rates: CADRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - CADRates
struct CADRates: Codable {
    let cad: ARSClass

    enum CodingKeys: String, CodingKey {
        case cad = "CAD"
    }
}

// MARK: - Chf
struct Chf: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.chf
    }
    
    var productID: String
    let rates: CHFRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - CHFRates
struct CHFRates: Codable {
    let chf: ARSClass

    enum CodingKeys: String, CodingKey {
        case chf = "CHF"
    }
}

// MARK: - Clp
struct Clp: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.clp
    }
    
    var productID: String
    let rates: CLPRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - CLPRates
struct CLPRates: Codable {
    let clp: ARSClass

    enum CodingKeys: String, CodingKey {
        case clp = "CLP"
    }
}

// MARK: - Cnh
struct Cnh: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.cnh
    }
    
    var productID: String
    let rates: CNHRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - CNHRates
struct CNHRates: Codable {
    let cnh: ARSClass

    enum CodingKeys: String, CodingKey {
        case cnh = "CNH"
    }
}

// MARK: - Cny
struct Cny: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.cny
    }
    
    var productID: String
    let rates: CNYRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - CNYRates
struct CNYRates: Codable {
    let cny: ARSClass

    enum CodingKeys: String, CodingKey {
        case cny = "CNY"
    }
}

// MARK: - Dkk
struct Dkk: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.dkk
    }
    
    var productID: String
    let rates: DKKRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - DKKRates
struct DKKRates: Codable {
    let dkk: ARSClass

    enum CodingKeys: String, CodingKey {
        case dkk = "DKK"
    }
}

// MARK: - Eur
struct Eur: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.eur
    }
    
    var productID: String
    let rates: EURRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - EURRates
struct EURRates: Codable {
    let eur: ARSClass

    enum CodingKeys: String, CodingKey {
        case eur = "EUR"
    }
}

// MARK: - Fjd
struct Fjd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.fjd
    }
    
    var productID: String
    let rates: FJDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - FJDRates
struct FJDRates: Codable {
    let fjd: ARSClass

    enum CodingKeys: String, CodingKey {
        case fjd = "FJD"
    }
}

// MARK: - Gbp
struct Gbp: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.gbp
    }
    
    var productID: String
    let rates: GBPRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - GBPRates
struct GBPRates: Codable {
    let gbp: ARSClass

    enum CodingKeys: String, CodingKey {
        case gbp = "GBP"
    }
}

// MARK: - Hkd
struct Hkd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.hkd
    }
    
    var productID: String
    let rates: HKDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - HKDRates
struct HKDRates: Codable {
    let hkd: ARSClass

    enum CodingKeys: String, CodingKey {
        case hkd = "HKD"
    }
}

// MARK: - Idr
struct Idr: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.idr
    }
    
    var productID: String
    let rates: IDRRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - IDRRates
struct IDRRates: Codable {
    let idr: ARSClass

    enum CodingKeys: String, CodingKey {
        case idr = "IDR"
    }
}

// MARK: - Inr
struct Inr: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.inr
    }
    
    var productID: String
    let rates: INRRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - INRRates
struct INRRates: Codable {
    let inr: ARSClass

    enum CodingKeys: String, CodingKey {
        case inr = "INR"
    }
}

// MARK: - Jpy
struct Jpy: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.jpy
    }
    
    var productID: String
    let rates: JPYRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - JPYRates
struct JPYRates: Codable {
    let jpy: ARSClass

    enum CodingKeys: String, CodingKey {
        case jpy = "JPY"
    }
}

// MARK: - Krw
struct Krw: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.krw
    }
    
    var productID: String
    let rates: KRWRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - KRWRates
struct KRWRates: Codable {
    let krw: ARSClass

    enum CodingKeys: String, CodingKey {
        case krw = "KRW"
    }
}

// MARK: - Lkr
struct Lkr: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.lkr
    }
    
    var productID: String
    let rates: LKRRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - LKRRates
struct LKRRates: Codable {
    let lkr: ARSClass

    enum CodingKeys: String, CodingKey {
        case lkr = "LKR"
    }
}

// MARK: - Myr
struct Myr: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.myr
    }
    
    var productID: String
    let rates: MYRRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - MYRRates
struct MYRRates: Codable {
    let myr: ARSClass

    enum CodingKeys: String, CodingKey {
        case myr = "MYR"
    }
}

// MARK: - Nok
struct Nok: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.nok
    }
    
    var productID: String
    let rates: NOKRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - NOKRates
struct NOKRates: Codable {
    let nok: ARSClass

    enum CodingKeys: String, CodingKey {
        case nok = "NOK"
    }
}

// MARK: - Nzd
struct Nzd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.nzd
    }
    
    var productID: String
    let rates: NZDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - NZDRates
struct NZDRates: Codable {
    let nzd: ARSClass

    enum CodingKeys: String, CodingKey {
        case nzd = "NZD"
    }
}

// MARK: - Pgk
struct Pgk: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.pgk
    }
    
    var productID: String
    let rates: PGKRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - PGKRates
struct PGKRates: Codable {
    let pgk: ARSClass

    enum CodingKeys: String, CodingKey {
        case pgk = "PGK"
    }
}

// MARK: - PHP
struct PHP: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.php
    }
    
    var productID: String
    let rates: PHPRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - PHPRates
struct PHPRates: Codable {
    let php: ARSClass

    enum CodingKeys: String, CodingKey {
        case php = "PHP"
    }
}

// MARK: - Pkr
struct Pkr: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.pkr
    }
    
    var productID: String
    let rates: PKRRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - PKRRates
struct PKRRates: Codable {
    let pkr: ARSClass

    enum CodingKeys: String, CodingKey {
        case pkr = "PKR"
    }
}

// MARK: - Sar
struct Sar: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.sar
    }
    
    var productID: String
    let rates: SARRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - SARRates
struct SARRates: Codable {
    let sar: ARSClass

    enum CodingKeys: String, CodingKey {
        case sar = "SAR"
    }
}

// MARK: - Sbd
struct Sbd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.sbd
    }
    
    var productID: String
    let rates: SBDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - SBDRates
struct SBDRates: Codable {
    let sbd: ARSClass

    enum CodingKeys: String, CodingKey {
        case sbd = "SBD"
    }
}

// MARK: - Sek
struct Sek: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.sek
    }
    
    var productID: String
    let rates: SEKRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - SEKRates
struct SEKRates: Codable {
    let sek: ARSClass

    enum CodingKeys: String, CodingKey {
        case sek = "SEK"
    }
}

// MARK: - Sgd
struct Sgd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.sgd
    }
    
    var productID: String
    let rates: SGDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - SGDRates
struct SGDRates: Codable {
    let sgd: ARSClass

    enum CodingKeys: String, CodingKey {
        case sgd = "SGD"
    }
}

// MARK: - Thb
struct Thb: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.thb
    }
    
    var productID: String
    let rates: THBRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - THBRates
struct THBRates: Codable {
    let thb: ARSClass

    enum CodingKeys: String, CodingKey {
        case thb = "THB"
    }
}

// MARK: - Top
struct Top: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.top
    }
    
    var productID: String
    let rates: TOPRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - TOPRates
struct TOPRates: Codable {
    let top: ARSClass

    enum CodingKeys: String, CodingKey {
        case top = "TOP"
    }
}

// MARK: - Twd
struct Twd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.twd
    }
    
    var productID: String
    let rates: TWDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - TWDRates
struct TWDRates: Codable {
    let twd: ARSClass

    enum CodingKeys: String, CodingKey {
        case twd = "TWD"
    }
}

// MARK: - Usd
struct Usd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.usd
    }
    
    var productID: String
    let rates: USDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - USDRates
struct USDRates: Codable {
    let usd: ARSClass

    enum CodingKeys: String, CodingKey {
        case usd = "USD"
    }
}

// MARK: - Vnd
struct Vnd: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.vnd
    }
    
    var productID: String
    let rates: VNDRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - VNDRates
struct VNDRates: Codable {
    let vnd: ARSClass

    enum CodingKeys: String, CodingKey {
        case vnd = "VND"
    }
}

// MARK: - Vuv
struct Vuv: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.vuv
    }
    
    var productID: String
    let rates: VUVRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - VUVRates
struct VUVRates: Codable {
    let vuv: ARSClass

    enum CodingKeys: String, CodingKey {
        case vuv = "VUV"
    }
}

// MARK: - Wst
struct Wst: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.wst
    }
    
    var productID: String
    let rates: WSTRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - WSTRates
struct WSTRates: Codable {
    let wst: ARSClass

    enum CodingKeys: String, CodingKey {
        case wst = "WST"
    }
}

// MARK: - Xau
struct Xau: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.xau
    }
    
    var productID: String
    let rates: XAURates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - XAURates
struct XAURates: Codable {
    let xau: ARSClass

    enum CodingKeys: String, CodingKey {
        case xau = "XAU"
    }
}

// MARK: - Xpf
struct Xpf: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.xpf
    }
    
    var productID: String
    let rates: XPFRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - XPFRates
struct XPFRates: Codable {
    let xpf: ARSClass

    enum CodingKeys: String, CodingKey {
        case xpf = "XPF"
    }
}

// MARK: - Zar
struct Zar: Codable, ProductsProtocol {
    func getProductsDetails() -> ProductsDetailsProtocol {
        return rates.zar
    }
    
    var productID: String
    let rates: ZARRates

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - ZARRates
struct ZARRates: Codable {
    let zar: ARSClass

    enum CodingKeys: String, CodingKey {
        case zar = "ZAR"
    }
}
