//
//  Helper.swift
//  Assessment
//
//  Created by Amol Bombe on 27/03/22.
//

import Foundation

enum CurrencyConstants {
    static let leftRightSpacing = 8.0
    static let topBottonSpacing = 16.0
    static let spacing = 8.0
    static let zero = 0.0
    static let selectCurrency = "select currency"
    static let currencyConverter = "Currency Converter"
    static let zeroText = "0"
    static let aud = "AUD"
}
