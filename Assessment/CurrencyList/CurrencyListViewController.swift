//
//  CurrencyListViewController.swift
//  Assessment
//
//  Created by Amol Bombe on 22/03/22.
//

import UIKit

struct ListViewModel {
    let title: String
}

class CurrencyListViewController: UITableViewController {
    
    typealias DidSelectItemClosure = (String, Int) -> Void
    private let selectionClosure: DidSelectItemClosure
    
    private let viewModel: [ListViewModel]
    
    init(viewModel: [ListViewModel], selection: @escaping DidSelectItemClosure) {
        self.viewModel = viewModel
        selectionClosure = selection
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = viewModel[indexPath.row].title

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectionClosure(viewModel[indexPath.row].title, indexPath.row)
    }
}
