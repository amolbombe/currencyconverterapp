//
//  CurrencyConverterViewController.swift
//  Assessment
//
//  Created by Amol Bombe on 21/03/22.
//

import UIKit

class CurrencyConverterViewController: UIViewController {
    
    private lazy var containerStack: UIStackView = {
        let containerStack = UIStackView()
        containerStack.axis = .vertical
        containerStack.distribution = .fill
        containerStack.translatesAutoresizingMaskIntoConstraints = false
        containerStack.spacing = CurrencyConstants.spacing
        return containerStack
    }()
    
    private lazy var audCurrencyStack: UIStackView = {
        let containerStack = UIStackView()
        containerStack.axis = .horizontal
        containerStack.distribution = .fillEqually
        containerStack.translatesAutoresizingMaskIntoConstraints = false
        containerStack.spacing = CurrencyConstants.spacing
        return containerStack
    }()
    
    private lazy var currencySelectionStack: UIStackView = {
        let containerStack = UIStackView()
        containerStack.axis = .horizontal
        containerStack.distribution = .fillEqually
        containerStack.translatesAutoresizingMaskIntoConstraints = false
        containerStack.spacing = CurrencyConstants.spacing
        return containerStack
    }()
    
    private lazy var amountInAUD: UILabel = {
        let label = UILabel()
        label.text = CurrencyConstants.zeroText
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var AUDLabel: UILabel = {
        let label = UILabel()
        label.text = CurrencyConstants.aud
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = CurrencyConstants.zeroText
        textField.backgroundColor = .gray.withAlphaComponent(0.2)
        textField.isUserInteractionEnabled = true
        textField.keyboardType = .numberPad
        textField.layer.cornerRadius = 5
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    private lazy var selectCurrency: UIButton = {
        let button = UIButton()
        button.backgroundColor = .black.withAlphaComponent(0.5)
        button.setTitle(CurrencyConstants.selectCurrency, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        return button
    }()
    
    var presenter: CurrencyConverterPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = CurrencyConstants.currencyConverter
        setUpUI()
        presenter?.getUpdatedData()
    }
    
    func setUpUI() {
        audCurrencyStack.addArrangedSubview(amountInAUD)
        audCurrencyStack.addArrangedSubview(AUDLabel)
        currencySelectionStack.addArrangedSubview(inputTextField)
        currencySelectionStack.addArrangedSubview(selectCurrency)
        containerStack.addArrangedSubview(audCurrencyStack)
        containerStack.addArrangedSubview(currencySelectionStack)
        view.addSubview(containerStack)
        
        NSLayoutConstraint.activate([
            containerStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: CurrencyConstants.leftRightSpacing),
            containerStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: CurrencyConstants.topBottonSpacing),
            containerStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -CurrencyConstants.leftRightSpacing)
        ])
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        presenter?.amountToConvertChanged(amount: textField.text)
    }
    
    @objc func buttonAction() {
        presenter?.selectCurrencyDidTapped()
    }
}

extension CurrencyConverterViewController: CurrencyConverterVC {
    func getViewController() -> UIViewController? {
        return self
    }
    
    func updateAUDLabelText(text: String) {
        amountInAUD.text = text
    }
    
    func updateSelectedCurrencyText(text: String) {
        selectCurrency.setTitle(text, for: .normal)
    }
    
    
}
