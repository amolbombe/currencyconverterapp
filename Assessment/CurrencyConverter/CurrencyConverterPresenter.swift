//
//  CurrencyConverterPresenter.swift
//  Assessment
//
//  Created by Amol Bombe on 21/03/22.
//

import Foundation

class CurrencyConverterPresenter {
    var view: CurrencyConverterVC?
    var interactor: CurrencyConverterInteractorProtocol?
    var router: CurrencyConverterRouterProtocol?

    init(view: CurrencyConverterVC, interactor: CurrencyConverterInteractorProtocol, router: CurrencyConverterRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

extension CurrencyConverterPresenter: CurrencyConverterPresenterProtocol {
    func amountToConvertChanged(amount: String?) {
        interactor?.updateAmountToConvert(amount: amount)
    }
    
    func displayCurrencyList(listViewModels: [ListViewModel]) {
        guard let viewController = view?.getViewController() else {
            return
        }
        router?.showCurrencyList(viewcontroller: viewController, listViewModels: listViewModels, selection: { [weak self] name, index in
            self?.interactor?.updateSelectedCurrencyRate(index: index, name: name)
        })
    }
    
    func getUpdatedData() {
        interactor?.fetchCurrencyData()
    }
    
    func updateAUDValue(value: String) {
        view?.updateAUDLabelText(text: value)
    }
    
    func updateSelectedCurrency(selectedCurrency: String) {
        view?.updateSelectedCurrencyText(text: selectedCurrency)
    }
    
    func selectCurrencyDidTapped() {
        interactor?.showCurrencyListSelction()
    }
}
