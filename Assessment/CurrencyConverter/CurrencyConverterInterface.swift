//
//  CurrencyConverterInterface.swift
//  Assessment
//
//  Created by Amol Bombe on 21/03/22.
//

import UIKit

//protocol

protocol CurrencyConverterInteractorProtocol {
    func fetchCurrencyData()
    func updateSelectedCurrencyRate(index: Int, name: String)
    func updateAmountToConvert(amount: String?)
    func showCurrencyListSelction()
}

protocol CurrencyConverterVC {
    func updateAUDLabelText(text: String)
    func updateSelectedCurrencyText(text: String)
    func getViewController()-> UIViewController?
}

protocol CurrencyConverterPresenterProtocol {
    func getUpdatedData()
    func updateAUDValue(value: String)
    func updateSelectedCurrency(selectedCurrency: String)
    func displayCurrencyList(listViewModels: [ListViewModel])
    func amountToConvertChanged(amount: String?)
    func selectCurrencyDidTapped()
}

protocol CurrencyConverterRouterProtocol {
    func showCurrencyList(viewcontroller: UIViewController,listViewModels: [ListViewModel], selection: @escaping(String, Int) -> Void)
}
