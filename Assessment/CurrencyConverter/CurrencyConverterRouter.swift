//
//  CurrencyConverterRouter.swift
//  Assessment
//
//  Created by Amol Bombe on 25/03/22.
//

import UIKit

class CurrencyConverterRouter {
}

extension CurrencyConverterRouter: CurrencyConverterRouterProtocol {
    func showCurrencyList(viewcontroller: UIViewController,listViewModels: [ListViewModel], selection: @escaping(String, Int) -> Void) {
        let vc = CurrencyListViewController(viewModel: listViewModels, selection: { [weak viewcontroller] name, index in
            viewcontroller?.dismiss(animated: true)
            selection(name, index)
        })
        
        viewcontroller.present(vc, animated: true)
    }
}
