//
//  CurrencyConverterInteractor.swift
//  Assessment
//
//  Created by Amol Bombe on 21/03/22.
//

import Foundation

class CurrencyConverterInteractor {
    
    var response: Response?
    var rate: Double = 0 {
            didSet {
                updateConvertedAmount()
            }
    }
    var amount: Double = 0 {
        didSet {
            updateConvertedAmount()
        }
    }
    var models: [ProductsProtocol]?
    var listViewModels: [ListViewModel]?
    weak var presenter: CurrencyConverterPresenter?
    
    
    func updateResponse() {
        self.models = self.response?.data.brands.wbc.portfolios.fx.products.getAllProductsArray()
        self.createListViewModels()
        if let name = self.listViewModels?[0].title {
            self.updateSelectedCurrencyRate(index: 0, name: name)
        }
    }
    
    func createListViewModels() {
        guard let models = models else {
            return
        }
        listViewModels = models.map({ product in
            let details = product.getProductsDetails()
            return ListViewModel(title: details.country + " " + details.currencyName)
        })
    }
    
    func updateConvertedAmount() {
        if amount < 0 || rate < 0 {
            presenter?.updateAUDValue(value: "N/A")
            return
        }
        let amt = amount/rate
        let amountToDisplay = String(format: "%.2f", amt)
        presenter?.updateAUDValue(value: amountToDisplay)
    }
}

extension CurrencyConverterInteractor: CurrencyConverterInteractorProtocol {
    
    func fetchCurrencyData() {
        NetworkHelper.shared.request(completion: { [weak self] (result: Result<Response, Error>) in
            guard let self = self else { return }
            switch result {
            case .success(let success):
                print("result \(success)------ : \(result)")
                self.response = success
                self.updateResponse()
            case .failure(_):
                break
            }
        })
    }
    
    func updateSelectedCurrencyRate(index: Int, name: String) {
        guard let models = models else {
            return
        }
        if let rateInInt = Double(models[index].getProductsDetails().buyTT) {
            self.rate = rateInInt
        } else if let sellRateInInt = Double(models[index].getProductsDetails().sellTT) {
            self.rate = sellRateInInt
        } else {
            return
        }
        presenter?.updateSelectedCurrency(selectedCurrency: name)
    }
    
    func updateAmountToConvert(amount: String?) {
        guard let amount = Double(amount ?? "") else {
            self.amount = 0
            return
        }
        self.amount = amount
    }
    
    func showCurrencyListSelction() {
        if let models = self.listViewModels {
            self.presenter?.displayCurrencyList(listViewModels: models)
        }
    }
}
