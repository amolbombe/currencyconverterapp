//
//  CurrencyConverterConfigurator.swift
//  Assessment
//
//  Created by Amol Bombe on 21/03/22.
//

import Foundation
import UIKit

class CurrencyConverterConfigurator {
    class func getCurrencyConverter()-> UIViewController {
        let viewController = CurrencyConverterViewController()
        let interactor = CurrencyConverterInteractor()
        let router = CurrencyConverterRouter()
        
        let presenter = CurrencyConverterPresenter(view: viewController, interactor: interactor, router: router)

        interactor.presenter = presenter
        viewController.presenter = presenter
        return viewController
    }
}
